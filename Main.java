/**
 * Created by Noroc Dinu-Marius on 01/10/2017.
 */


/* Mapper
 *
 * On utilisera un fichier .csv contentant notre table.
 * En input nous allons prendre un un id qui sera la key et l'ensemble des colonnes qui sera la value.
 * La séparation est faite par des virgules.
 * Input => Output :
 * Map(k1,v1)=>list(k2,v2)
 *
 */
        public class mapper{
            @Override
            public void map(Object key, Iterable<Text> value, Context context) {
                Text Mapkey = new Text;
                String line = value.toString();
                String[] values=line.split(",");
                for (int i = 0; i < values.length; i++) {
                    Text MapValue = new Text(number.trim());
                    outputKey++;
                    context.write(MapKey, MapValue);
                }}}
/* Reduce
 *
 * Input => Output pour le reduce : Reduce(k2,list(v2))=>list(k3,v3)
 * Apres l'etape de Sort & Shuffle le reduce va récuperer une liste avec les valeurs de chaque map triée.
 *
 */
class reducer{
    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) {
        Text ReducedText = new Text();
        for (String value: values){
        }
          context.write(key, colonne);}}

/* Output
*
*En output on aura un fichier .csv contant le pivot de la table initiale.
*
*/

